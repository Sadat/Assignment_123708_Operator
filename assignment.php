<?php


$a = ($b = 4) + 5; // $a is equal to 9 now, and $b has been set to 4.
echo $a;
echo "<hr>";
echo $b;
echo "<hr>";
$a = 3;
$a += 5; // sets $a to 8, as if we had said: $a = $a + 5;
$b = "Hello ";
$b .= "There!";
echo $b;

?>